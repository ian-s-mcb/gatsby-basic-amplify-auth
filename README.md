# Basic Gatbsy JS / AWS Amplify app with authentication

### Using this starter

```bash
npm install -g gatsby-cli
npm install -g @aws-amplify/cli@multienv
gatsby new gatsby-basic-amplify-auth https://gitlab.com/ian-s-mcb/gatsby-basic-amplify-auth
cd gatsby-basic-amplify-auth
amplify configure
amplify init
amplify add auth
amplify push
git add amplify
git commit -m "Add initial Amplify files"
yarn develop # Runs the app locally
```

### How this starter was written

This starter was based off [another starter][original-starter] that
worked reasonably well, but had some unnecessary cruft that I addressed
here.

Fixed cruft:
* Large repo size (37 MiB) because it contained large .gif files
* Hardcoded the AWS resource names to `my-aws-project` because it contained an `amplify` directory
* Outdated node dependencies

If you prefer to use that other starter and apply my fixes yourself, the
following commands will get you there:

```bash
# Copy starter code from the GitHub user dabit3
git clone https://github.com/dabit3/gatsby-auth-starter-aws-amplify gatsby-basic-amplify-auth
cd gatsby-basic-amplify-auth
rm src/images/*.gif
echo '# Basic Gatbsy JS / AWS Amplify app with authentication' > README.md
rm -rf .git amplify
git init
rm package.json package-lock.json yarn.lock

# Create `package.json`
yarn init
# <Interactive commands below>
# Run vim package.json
# Add the following block
#  "scripts": {
#    "build": "gatsby build",
#    "develop": "gatsby develop",
#    "test": "echo \"Error: no test specified\" && exit 1"
#  }
# Add newest version of dependencies
yarn add aws-amplify aws-amplify-react gatsby gatsby-plugin-manifest gatsby-plugin-offline gatsby-plugin-react-helmet react react-dom react-helmet

# Commit 
git add .
git commit -m "Initial commit"
```

[original-starter]: https://github.com/dabit3/gatsby-auth-starter-aws-amplify
